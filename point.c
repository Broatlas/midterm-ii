#include<stdio.h>
#include<stdlib.h>
/**********************/
struct Point3D {
  float x, y, z;
};
/****
function prototypes
***/
void displaypoint(struct Point3D *p);
/**/


/*
Main
*/
int main(void){

  struct Point3D vertex;
  struct Point3D *p;

  vertex.x = 10.5;
  vertex.y = 10.5;
  vertex.z = 10.5;
  p=malloc(sizeof(*p));
  p->x= -10.5;
  p->y= -10.5;
  p->z= -10.5;
  displaypoint(p);
  displaypoint(&vertex);

  printf(" SIZE OF vertex %lu\n", sizeof(vertex));
  printf(" SIZE OF p %lu\n", sizeof(p) );
  free(p);

  return 0;
}

/**************************************************************************/

void displaypoint(struct Point3D *p){
printf("x:%f\n",p->x);
printf("y:%f\n",p->y);
printf("z:%f\n",p->z);
}
