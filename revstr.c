#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "display_usage.h"
#define MAX_WORD 45

/*function prototypes*/
void revstr( char * s);
int reverseFile(char * filein, char *fileout);
/**********************************************
************************************************/
int main(int argc, char *argv[]){
  char cflag[MAX_WORD] = "filename";
  if((argv[1] != NULL)&&(argv[2] != NULL )){
  reverseFile(argv[1], argv[2]);
}else{
  display_usage(cflag, cflag);
}

    return 0;
}
/*************************************************************************/
/* Take in a char pointer and reverses the word*/
/************************************************************************/
void revstr(char *s){
  char* left = s;
  char* right = left + strlen(s) - 1;
  char tmp;
  while(left < right){
    tmp = *left;
    *left++ = *right;
    *right-- = tmp;
  }
  printf("%s\n",s);
}
/*************************************************************************/
/* Write a small C-like function reverseFile( ) that reverses each line of a file, and writes the new file into a
new file. Your function should return 0 if it was able to reverse the file or EXIT_FAILURE if it encountered any
problems.*/
/************************************************************************/
int reverseFile(char * filein, char *fileout){
  char *line;
  FILE *fp, *fp2;

  if((fp = fopen(filein, "r")) == NULL) {
    perror("fopen");
    exit(EXIT_FAILURE);
  }
  fp2 = fopen(fileout, "w");

  if((line = (char *)malloc (sizeof(char) * MAX_WORD)) == NULL){
  perror("malloc line");
  exit(EXIT_FAILURE);
}
while((fgets(line, MAX_WORD, fp))!= NULL){
  line[strlen(line)-1] = '\0';
  printf("%s\n",line);
  revstr(line);
  fputs(line, fp2);
}
return 0;
}
